import Vue from 'vue';

import {
  ValidationObserver,
  ValidationProvider,
  extend,
  setInteractionMode,
  localize,
} from 'vee-validate';
import VueTheMask from 'vue-the-mask';
import ru from 'vee-validate/dist/locale/ru.json';
import * as rules from 'vee-validate/dist/rules';
import {
  alphaRule,
  alphaNumRule,
  minMax,
  settedLength,
  dateRule,
  dateCompare,
  changedRequired,
} from '@/src/config/validationRules';

Object.keys(rules).forEach((rule) => {
  extend(rule, rules[rule]);
});
localize('ru', ru);

extend('minmax', minMax);
extend('alphaRu', alphaRule);
extend('alphaNumRu', alphaNumRule);
extend('length', settedLength);
extend('date', dateRule);
extend('dateCompare', dateCompare);
extend('required', changedRequired);

setInteractionMode('lazy');

Vue.use({
  install(Vue) {
    Vue.component('ValidationObserver', ValidationObserver);
    Vue.component('ValidationProvider', ValidationProvider);
  },
});
Vue.use(VueTheMask);
