import CInput from '@/src/ui/CInput';

console.log(CInput);

export default {
  title: 'UI/CInput',
  component: CInput,
  argTypes: {
    type: {
      control: { type: 'select' },
      options: ['number', 'text', 'password', 'email'],
    },
    size: {
      control: { type: 'select' },
      options: ['s', 'm'],
    },
    theme: {
      control: { type: 'select' },
      options: ['bordered', 'no-border'],
    },
    inputValue: { action: 'input' },
  },
};

const CInputStories = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { CInput },
  template:
    `<CInput ` +
    'v-model="inputValue" ' +
    'class="stories__c-input" ' +
    ':placeholder="placeholder" ' +
    ':type="type" ' +
    ':size="size" ' +
    ':error="error" ' +
    ':theme="theme" ' +
    '/>',
});

export const Primary = CInputStories.bind({});
Primary.args = {
  type: 'number',
  size: 's',
  placeholder: 'Номер телефона',
  error: '',
  inputValue: '',
};
