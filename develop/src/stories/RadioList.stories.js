import CRadioList from '@/src/ui/CRadioList';

export default {
  title: 'Components/Radio List',
  component: CRadioList,
  argTypes: {},
};

const RadioListTemplate = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { CRadioList },
  template:
    '<CRadioList v-model="picked" input-name="radioList" :items="items"/>',
});

export const Primary = RadioListTemplate.bind({});
Primary.args = {
  items: [
    {
      id: 0,
      name: 'Option 1',
    },
    {
      id: 1,
      name: 'Option 2',
    },
    {
      id: 2,
      name: 'Option 3',
    },
    {
      id: 3,
      name: 'Option 4',
    },
  ],
  picked: 'Option 2',
};
