import CombinedInput from '@/src/components/CombinedInput';

export default {
  title: 'Components/CombinedInput',
  component: CombinedInput,
  argTypes: {
    type1: {
      control: { type: 'select' },
      options: ['number', 'text', 'password', 'email'],
    },
    type2: {
      control: { type: 'select' },
      options: ['number', 'text', 'password', 'email'],
    },
    type3: {
      control: { type: 'select' },
      options: ['number', 'text', 'password', 'email'],
    },
    size: {
      control: { type: 'select' },
      options: ['s', 'm'],
    },
    inputValue: { action: 'input' },
  },
};

const CombinedInputStories = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { CombinedInput },
  template: `
    <CombinedInput class="stories__combined-inputs" :error="error">
      <template #input1>
        <CInput
          class="stories__combined-input"
          v-model="inputValue1"
          :placeholder="placeholder"
          :type="type1"
          :size="size"
          :error="error"
          theme="no-border"
        />
      </template>
      <template #input2>
        <CInput
          class="stories__combined-input"
          v-model="inputValue2"
          :placeholder="placeholder"
          :type="type2"
          :size="size"
          :error="error"
          theme="no-border"
        />
      </template>
      <template #input3>
        <CInput
          class="stories__combined-input"
          v-model="inputValue3"
          :placeholder="placeholder"
          :type="type3"
          :size="size"
          :error="error"
          theme="no-border"
        />
      </template>
    </CombinedInput>
    `,
});

export const Primary = CombinedInputStories.bind({});
Primary.args = {
  type: 'number',
  size: 's',
  placeholder: 'Номер телефона',
  error: 'huy',
  inputValue1: '',
  inputValue2: '',
  inputValue3: '',
};
