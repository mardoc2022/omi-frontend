import CRadioButtonsList from '@/src/ui/CRadioButtonsList';

export default {
  title: 'Components/Radio Buttons List',
  component: CRadioButtonsList,
  argTypes: {},
};

const CRadioButtonsListTemplate = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { CRadioButtonsList },
  template:
    '<CRadioButtonsList v-model="localValue" :buttons="items" inputs-name="buttons"/>',
});

export const Primary = CRadioButtonsListTemplate.bind({});
Primary.args = {
  items: [
    {
      text: 'Button 1',
    },
    {
      text: 'Button 2',
    },
  ],
};
