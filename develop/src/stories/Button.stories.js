import CButton from '@/src/ui/CButton';

export default {
  title: 'UI/Button',
  component: CButton,
  argTypes: {
    theme: {
      control: { type: 'select' },
      options: [
        'default',
        'bordered-black',
        'bordered-transparent-black',
        'bordered-white',
        'transparent',
        'bordered-disabled',
        'default-disabled',
      ],
    },
    onClick: { action: 'hello' },
  },
};

const ButtonStories = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { CButton },
  template: '<CButton @click="onClick" :theme="theme">{{label}}</CButton>',
});

export const Primary = ButtonStories.bind({});
Primary.args = {
  label: 'CButton',
  theme: 'default',
};

export const BorderedBlack = ButtonStories.bind({});
BorderedBlack.args = {
  label: 'CButton',
  theme: 'bordered-black',
};

BorderedBlack.parameters = {
  backgrounds: { default: 'light' },
};

export const BorderedTransparentBlack = ButtonStories.bind({});
BorderedTransparentBlack.args = {
  label: 'CButton',
  theme: 'bordered-transparent-black',
};

BorderedTransparentBlack.parameters = {
  backgrounds: { default: 'light' },
};

export const BorderedWhite = ButtonStories.bind({});
BorderedWhite.args = {
  label: 'CButton',
  theme: 'bordered-white',
};

BorderedWhite.parameters = {
  backgrounds: { default: 'dark' },
};

export const Transparent = ButtonStories.bind({});
Transparent.args = {
  label: 'CButton',
  theme: 'transparent',
};
