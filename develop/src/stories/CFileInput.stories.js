import CFileInput from '@/src/ui/CFileInput';

export default {
  title: 'UI/File Input',
  component: CFileInput,
  argTypes: {},
};

const CFileInputTemplate = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { CFileInput },
  template:
    '<CFileInput class="stories__file-input" :error="error" :value="localValue" :input-name="inputName" :message="message"/>',
});

export const Primary = CFileInputTemplate.bind({});
Primary.args = {
  error: 'Обязательно',
  localValue: '',
  inputName: 'fileInput',
  message: 'Ваше фото 3 на 4',
};
