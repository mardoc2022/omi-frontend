import { ValidationProvider } from 'vee-validate';

export default function (component) {
  return {
    name: `${component.name}WithValidation`,
    props: {
      ...ValidationProvider.options.props, // validation props
      ...component.props, // component props
    },
    computed: {
      validationProps() {
        return {
          ...this.$props,
          slim: true,
        };
      },
    },
    methods: {
      slotProps(validationProps) {
        return {
          ...this.$attrs,
          ...this.$props,
          error: validationProps.errors[0],
        };
      },
    },
    render(h) {
      return h(ValidationProvider, {
        props: this.validationProps,
        scopedSlots: {
          default: (validationProps) =>
            h(component, {
              props: this.slotProps(validationProps),
              on: this.$listeners,
              scopedSlots: this.$scopedSlots, // pass named slots to component
            }),
        },
      });
    },
  };
}
