export const em = (pixels, fontSize = 16) => {
  return pixels / fontSize + 'em';
};
