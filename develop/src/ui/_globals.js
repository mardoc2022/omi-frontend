import withValidation from '@/src/tools/withValidation';
import CButton from '@/src/ui/CButton';
import CPyramid from '@/src/ui/CPyramid';
import CCheckBox from '@/src/ui/CCheckBox';
import CSearch from '@/src/ui/CSearch';
import CIcon from '@/src/ui/CIcon';
import CRadioList from '@/src/ui/CRadioList';
import CLink from '@/src/ui/CLink';
import CInput from '@/src/ui/CInput';
import CRadioButton from '@/src/ui/CRadioButton';
import CFileInput from '@/src/ui/CFileInput';
import CProgress from '@/src/ui/CProgress';
import CSteps from '@/src/ui/CSteps';
import CRadioButtonsList from '@/src/ui/CRadioButtonsList';
import CDot from '@/src/ui/CDot';
import CDateInput from '@/src/ui/CDateInput';
import CAccordionInput from '@/src/ui/CАccordionInput';

const VueUI = {
  install(Vue) {
    Vue.component('CButton', CButton);
    Vue.component('CPyramid', CPyramid);
    Vue.component('CCheckBox', CCheckBox);
    Vue.component('CSearch', CSearch);
    Vue.component('CIcon', CIcon);
    Vue.component('CRadioList', CRadioList);
    Vue.component('CLink', CLink);
    Vue.component('CInput', withValidation(CInput));
    Vue.component('CRadioButton', CRadioButton);
    Vue.component('CFileInput', CFileInput);
    Vue.component('CProgress', CProgress);
    Vue.component('CSteps', CSteps);
    Vue.component('CRadioButtonsList', CRadioButtonsList);
    Vue.component('CDot', CDot);
    Vue.component('CDateInput', withValidation(CDateInput));
    Vue.component('CAccordionInput', CAccordionInput);
  },
};

export default VueUI;
