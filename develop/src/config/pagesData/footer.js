export const footer = {
  text: `Учебный центр специалистов морского транспорта (УЦСМТ) осуществляет
          подготовку плавсостава согласно требованиям Международной конвенции о
          подготовке, дипломировании моряков и несении вахты, а также занимается
          подготовкой, переподготовкой и повышением квалификации рядового
          плавсостава. Общее количество предлагаемых учебно-тренировочных курсов
          составляет более 51. Все виды подготовки отвечают требованиям
          национальных и международных стандартов, и признаны Государственной
          службой Украины по безопасности на транспорте, что подтверждено
          соответствующими Протоколами о соответствии. Подготовка рабочих
          профессий лицензирована Министерством образования и науки Украины.
          Форма документов, которые выдаются Центром соответствует национальным
          и международным требованиям. В Центре внедрена Система управления
          качеством, которая согласно требованиям действующих в Украине
          нормативно-правовых документов отвечает требованиям ГСТУ ISO
          9001-2008, сертифицирована Регистром судоходства Украины.`,
};
