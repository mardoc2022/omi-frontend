export const contacts = {
  citiesData: [
    {
      id: 0,
      name: 'Одесса',
      address:
        '65014, г. Одесса\n' +
        'ул. Канатная 42, оф. 325\n' +
        '+380 (48) 737-30-58',
      schedule: [
        {
          day: 'Понедельник',
          time: '9:00 - 17:00',
        },
        {
          day: 'Вторник',
          time: '9:00 - 17:00',
        },
        {
          day: 'Среда',
          time: '9:00 - 17:00',
        },
        {
          day: 'Четверг',
          time: '9:00 - 17:00',
        },
        {
          day: 'Пятница',
          time: '9:00 - 17:00',
        },
        {
          day: 'Суббота',
          time: 'выходной',
        },
        {
          day: 'Воскресенье',
          time: 'выходной',
        },
      ],
      phoneNumber: '+380(48) 73-73-058',
      socialLinks: {
        facebook: 'https://www.youtube.com/watch?v=laLyN9eftY4',
        instagram: 'https://www.youtube.com/watch?v=laLyN9eftY4',
      },
    },

    {
      id: 1,
      name: 'Херсон',
      address: `65014, г. Херсон ул.Канатная 42, оф. 325 +380 (48) 737-30-58`,
      schedule: [
        {
          day: 'Понедельник',
          time: '7:00 - 17:00',
        },
        {
          day: 'Вторник',
          time: '7:00 - 17:00',
        },
        {
          day: 'Среда',
          time: '7:00 - 17:00',
        },
        {
          day: 'Четверг',
          time: '7:00 - 17:00',
        },
        {
          day: 'Пятница',
          time: '7:00 - 17:00',
        },
        {
          day: 'Суббота',
          time: 'выходной',
        },
        {
          day: 'Воскресенье',
          time: 'выходной',
        },
      ],
      phoneNumber: '+380 (00) 000-00-00',
      socialLinks: {
        facebook: 'facebook-link',
        instagram: 'instagram-link',
      },
    },
  ],
};
