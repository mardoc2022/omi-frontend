import routes from '@/src/config/constants/routes';

export const cabinetCategories = [
  {
    text: 'Заявки',
    iconName: 'applications',
    link: routes.USER_APPLICATIONS,
  },
  {
    text: 'Курсы',
    iconName: 'courses',
    link: routes.USER_COURSES,
  },
  {
    text: 'Сертификаты',
    iconName: 'certificates',
    notification: {
      type: 'message',
      text: '1',
    },
    link: routes.USER_CERTIFICATES,
  },
  {
    text: 'Анкета',
    iconName: 'questionnaire',
    notification: {
      type: 'error',
    },
    link: routes.USER_QUESTIONNAIRE_STEPS,
  },
];
