export const questionnaireSteps = [
  {
    name: 'Анкета',
    state: 'done',
  },
  {
    name: 'Паспорта',
    state: 'done',
  },
  {
    name: 'Образование',
    state: 'rejected',
  },
  {
    name: 'Стаж',
    state: 'done',
  },
  {
    name: 'Сертификаты',
    state: 'not-done',
  },
  {
    name: 'Справки',
    state: 'not-done',
  },
];
