import routes from '@/src/config/constants/routes';

export const steps = [
  {
    name: 'Анкета',
    path: routes.QUESTIONNAIRE,
  },
  {
    name: 'Паспорта',
    path: routes.PASSPORTS,
  },
  {
    name: 'Образование',
    path: routes.EDUCATION,
  },
  {
    name: 'Стаж',
    path: routes.EXPERIENCE,
  },
  {
    name: 'Сертификаты',
    path: routes.CERTIFICATES,
  },
];
