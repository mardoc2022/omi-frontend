export const questionnaireData = {
  headingsTemplate: 'Анкета',
  headingBlock: 'Оновная информация',
  inputsData: [
    {
      placeholder: 'Ваше фото 3х4',
      inputName: 'photo',
    },
    {
      placeholder: 'Фамилия',
      inputName: 'secondName',
    },
    {
      placeholder: 'Имя',
      inputName: 'firstName',
    },
    {
      placeholder: 'Отчество',
      inputName: 'middleName',
    },
  ],
};
