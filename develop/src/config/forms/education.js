export const diploma = {
  inputsData: [
    {
      placeholder: 'Номер документа',
      inputName: 'number',
    },
    {
      placeholder: 'Название',
      inputName: 'institution_name',
    },
    {
      placeholder: 'Квалификация',
      inputName: 'qualification',
    },
  ],
  combinedInputs: [
    {
      placeholder: 'Дата начала',
      inputName: 'start_date',
    },
    {
      placeholder: 'Дата окончания',
      inputName: 'end_date',
    },
  ],
};
