export const passportData = {
  headingsTemplate: ['Гражданский', 'Заграничный', 'Паспорт моряка'],
  civil: {
    headingsBlock: ['Тип паспорта', 'Паспортные данные', 'Фотографии паспорта'],
    inputNames: ['passportType', 'serialNumber', 'issuedBy', 'date'],
    buttons: [
      {
        text: 'Бумажный',
      },
      {
        text: 'ID - карта',
      },
    ],
    placeholders: ['Серия и номер', 'Кем выдан', 'Дата выдачи'],
    photos: [
      {
        file: '',
        inputName: 'passportMain',
        placeholder: 'Главный разворот',
      },
      {
        file: '',
        inputName: 'registration',
        placeholder: 'Прописка',
      },
      {
        file: '',
        inputName: 'familyStatus',
        placeholder: 'Семейное положение',
      },
      {
        file: '',
        inputName: 'children',
        placeholder: 'Дети',
      },
      {
        file: '',
        inputName: 'additionalPage',
        placeholder: 'Дополнительная страница',
      },
    ],
  },
  international: {
    headingsBlock: ['Паспортные данные', 'Фотографии паспорта'],
    inputNames: ['serialNumber', 'issuedBy', 'date', 'expirationDate'],
    placeholders: [
      'Серия и номер',
      'Кем выдан',
      'Дата выдачи',
      'Дата оканчания',
    ],
    photos: [
      {
        file: '',
        inputName: 'passportMain',
        placeholder: 'Главный разворот',
      },
      {
        file: '',
        inputName: 'registration',
        placeholder: 'Прописка',
      },
      {
        file: '',
        inputName: 'familyStatus',
        placeholder: 'Семейное положение',
      },
      {
        file: '',
        inputName: 'children',
        placeholder: 'Дети',
      },
      {
        file: '',
        inputName: 'additionalPage',
        placeholder: 'Дополнительная страница',
      },
    ],
  },
  seaman: {
    headingsBlock: ['Паспортные данные', 'Фотографии паспорта'],
    inputNames: ['serialNumber', 'issuedBy', 'date', 'expirationDate'],
    placeholders: [
      'Серия и номер',
      'Кем выдан',
      'Дата выдачи',
      'Дата оканчания',
    ],
    photos: [
      {
        file: '',
        inputName: 'passportMain',
        placeholder: 'Главный разворот',
      },
      {
        file: '',
        inputName: 'registration',
        placeholder: 'Прописка',
      },
      {
        file: '',
        inputName: 'familyStatus',
        placeholder: 'Семейное положение',
      },
      {
        file: '',
        inputName: 'children',
        placeholder: 'Дети',
      },
      {
        file: '',
        inputName: 'additionalPage',
        placeholder: 'Дополнительная страница',
      },
    ],
  },
};
