export const experience = {
  inputsData: [
    {
      placeholder: 'Номер документа',
      inputName: 'number',
    },
    {
      placeholder: 'Ваше звание',
      inputName: 'rank',
    },
    {
      placeholder: 'Ваша должность',
      inputName: 'position',
    },
  ],
};
