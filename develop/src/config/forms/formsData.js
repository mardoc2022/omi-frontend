export const QUESTIONNAIRE_DATA = {
  first_name: '',
  second_name: '',
  middle_name: '',
  personal_photo: '',
};

export const CIVIL_PASSPORT_DATA = {
  passport_type: '',
  series_number: '',
  issuing_authority: '',
  receiving_date: '',
  main_reversal_photo: '',
  registration_photo: '',
  family_status_photo: '',
  children_photo: '',
  additional_page_photo: '',
  main_side_photo: '',
  back_side_photo: '',
};

export const INTERNATIONAL_PASSPORT_DATA = {
  number: '',
  issuing_authority: '',
  receiving_date: '',
  expiration_date: '',
  main_side_photo: '',
};

export const SEAMAN_PASSPORT_DATA = {
  number: '',
  issuing_authority: '',
  receiving_date: '',
  expiration_date: '',
  main_side_photo: '',
};

export const DIPLOMA = {
  number: '',
  institution_name: '',
  qualification: '',
  start_date: '',
  end_date: '',
  photos: ['', '', '', '', ''],
};

export const EXPERIENCE = {
  number: '',
  rank: '',
  position: '',
  photos: ['', '', '', '', ''],
};

export const CERTIFICATES = {
  number: '',
  institution_name: '',
  qualification: '',
  start_date: '',
  end_date: '',
  photos: ['', '', '', '', ''],
};
