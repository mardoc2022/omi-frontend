import routes from '@/src/config/constants/routes';

export const links = [
  {
    name: 'О нас',
    target: routes.ABOUT_US,
  },
  {
    name: 'Курсы',
    target: routes.COURSES,
  },
  {
    name: 'Контакты',
    target: routes.CONTACTS,
  },
];
