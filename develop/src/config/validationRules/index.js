import { alpha, alpha_num, required } from 'vee-validate/dist/rules';

export const minMax = {
  validate(value, { min, max }) {
    return value.length >= min && value.length <= max;
  },
  params: ['min', 'max'],
  message(field, { min, max, _value_ }) {
    const value = _value_.length;

    if (value > max) {
      return `Не более ${max} символов`;
    } else if (value < min) {
      return `Не менее ${min} символов`;
    }
  },
};

export const settedLength = {
  validate(value, { length }) {
    return value.length === parseInt(length);
  },
  params: ['length'],
  message: `Максимальная длинна {length} символов`,
};

export const alphaRule = {
  ...alpha,
  message: `Только кирилица`,
};

export const changedRequired = {
  ...required,
  message: `Обязательно`,
};

export const alphaNumRule = {
  ...alpha_num,
  message: `Только кирилица и цифры`,
};

export const dateRule = {
  validate(value, { addition }) {
    const day = parseInt(value.slice(0, 2));
    const month = parseInt(value.slice(2, 4));
    const year = parseInt(value.slice(4));
    const nowYear = new Date().getFullYear();
    const nowMonth = new Date().getMonth() + 1;
    const nowDay = new Date().getDate();
    const wrongFormatMessage = 'Неверный формат (dd.mm.yyyy)';
    const wrongDateMessage = 'Некорректные даты';

    if (day <= 0 || day > 31) return wrongFormatMessage;
    if (month <= 0 || month > 12) return wrongFormatMessage;
    if (year <= 0) return wrongFormatMessage;

    if (addition === 'fromToday') {
      if (year > nowYear) {
        return wrongDateMessage;
      } else if (year === nowYear) {
        if (month > nowMonth) {
          return wrongDateMessage;
        } else if (month === nowMonth) {
          if (day > nowDay) {
            return wrongDateMessage;
          }
        }
      }
    }

    return true;
  },
  params: ['addition'],
};

export const dateCompare = {
  validate(value, { target }) {
    const receiveDate = new Date(value);
    const expirationDate = new Date(target);

    if (receiveDate >= expirationDate) return 'Некорректные даты';

    return true;
  },
  params: ['target'],
};
