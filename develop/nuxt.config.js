const nodeEnv = process.env.NODE_ENV;

export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  target: 'static',
  mode: 'universal',
  head: {
    title: 'develop',
    htmlAttrs: {
      lang: 'en',
    },
    meta: [
      {charset: 'utf-8'},
      {name: 'viewport', content: 'width=device-width, initial-scale=1'},
      {hid: 'description', name: 'description', content: ''},
      {name: 'format-detection', content: 'telephone=no'},
    ],
    link: [
      {rel: 'icon', type: 'image/x-icon', href: '/favicon.ico'},
      {
        rel: 'preload',
        type: 'font/tff',
        as: 'font',
        href: '/fonts/centurygothic_bold.ttf',
      },
    ],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ['@/styles/app.scss', '@/styles/app-fonts.scss'],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: ['@plugins/ui', {src: '@/plugins/validations', mode: 'client'}],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {},

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    analyze: false,
    extractCSS: nodeEnv === 'production',
    cssSourceMap: nodeEnv === 'development',
    // transpile: [], // Transpile dependencies, example: 'swiper'
    babel: {
      presets({isServer}, [preset, options]) {
        return [
          [
            preset,
            {
              ...options,
              targets: isServer ? {node: '10'} : {ie: '11'},
            },
          ],
        ];
      },
    },
    transpile: ['vee-validate/dist/rules', 'gsap'],
    extend(config) {
      config.module.rules
        .filter((r) => r.test.toString().includes('svg'))
        .forEach((r) => {
          r.test = /\.(png|jpe?g|gif)$/;
        });
      // config.resolve.alias.vue = 'vue/dist/vue.common';
      config.module.rules.push({
        test: /\.svg$/,
        loader: 'svg-loader',
        exclude: /node_modules/,
      });
    },
  },
};
