module.exports = {
  apps: [
    {
      name: 'omi',
      script: 'yarn',
      args: ['start', '--port=3032'],
      env: {
        NODE_ENV: 'development',
        TEST: 'development',
      },
    },
  ],
};
