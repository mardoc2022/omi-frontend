import '../styles/stories.scss';
import Vue from 'vue';
import UI from '../src/ui/_globals';

Vue.use(UI);

export const parameters = {
  actions: { argTypesRegex: '^on[A-Z].*' },
  backgrounds: {
    default: 'twitter',
    values: [
      {
        name: 'dark',
        value: '#2c2e31',
      },
      {
        name: 'light',
        value: '#a7a7a7',
      },
    ],
  },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
};
